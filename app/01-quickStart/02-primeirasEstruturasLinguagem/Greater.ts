class Greater{
  greeting: string;

  constructor(message: string){
    this.greeting = message;
  }

  greet(){
    return this.greeting;
  }

}

//É possível adicionar código javaScript conforme abaixo

let greeter = new Greater("World");
alert(greeter.greet());
