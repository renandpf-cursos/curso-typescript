class Greeter<T>{
  greeting: T;

  constructor(param: T){
    this.greeting = param;
  }

  greet(){
    return this.greeting;
  }
}

//TESTE
let greeter = new Greeter<string>("Teste!");
//let greeter2 = new Greeter<number>("Teste!");//Ocorre erro de compilação
let greeter3 = new Greeter<number>(15);
