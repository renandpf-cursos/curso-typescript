class Animal{

  //Desta forma, estamos declarando uma propriedade ("name") na classe Animal
  //Também poderia colocar na classe
  constructor(public name: string){

  }

  //Desta forma estamos definin do um valor default para o parâmetro
  public move(distanceInMeters: number = 0){
    //Esta é uma forma de concatenar string: utilnzando crase
    //console.log(`$this(name) moved $(distanceInMeters)m`);
    console.log(this.name + ' moved ' + distanceInMeters + 'm');//Concatenação padrão
  }
}

class Snake extends Animal{
  constructor(name: string){
    super(name);
  }

  //Sobrescrevendo...
  public move(distanceInMeters: number = 5){
    console.log("Slithering...")
    super.move(distanceInMeters);
  }
}

class Horse extends Animal{
  constructor(name: string){
    super(name);
  }

  //Sobrescrevendo...
  public move(distanceInMeters: number = 45){
    console.log("Galloping...")
    super.move(distanceInMeters);
  }
}


//TESTES
let sammy = new Snake("Sammy the Pyton");
let tommy: Animal = new Horse("Tommy the Palomio");

sammy.move();//Passando o valor default
tommy.move();
tommy.move(34);
