//"var" não respeita escopo. Evitar utilizar
var variavel_a = 'Teste'//Evitar utilizar "var"
variavel_a = 'AA';//Permite alterar

const constante_a = 'TESTE';
//constante_a = '';//Ocorre erro

//"let" respeita escopo.
let variavel_b = 'Teste b';
variavel_b = 'Teste B';
