class Pessoa{
  nome: string;
  idade: number;
  cpf: string;

  constructor(nome:string, idade:number, cpf:string){
    this.nome = nome;
    this.idade = idade;
    this.cpf = cpf;
  }


}

class Carro{
  nome: string;
  cor: string;
  dono:Pessoa;

  constructor(nome:string, cor:string, dono:Pessoa){
    this.nome = nome;
    this.cor = cor;
    this.dono = dono;
  }

}

let renan = new Pessoa("Renan", 37, "088");
let carroRenan = new Carro("Fiesta", "Branco", renan);

let carros:Carro[] = [carroRenan, carroRenan];

carros[0].dono.nome;
