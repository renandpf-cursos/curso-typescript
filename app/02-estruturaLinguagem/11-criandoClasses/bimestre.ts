class Bimestre{

  //Pode ser declarado aqui ou no construtor
  private notaFinal:number = 0

  constructor(
    public nota1:number = 0,
    public nota2:number = 0,
    public nota3:number = 0,
    public nota4:number = 0){
  }

  public soma(
      nota1?:number,
      nota2?:number,
      nota3?:number,
      nota4?:number): number{

        if(isNaN(nota1) && isNaN(nota2) && isNaN(nota3) && isNaN(nota4)){
          return this.nota1 + this.nota2 + this.nota3 + this.nota4;
        }

        return nota1 + nota2 + nota3 + nota4;
  }

  public getNotaFinal():number{
    return this.notaFinal;
  }
}

let notasBimestre1 = [7,10,9,8];

let bimestre:Bimestre = new Bimestre(...notasBimestre1);
//Outras forma
//let bimestre:Bimestre = new Bimestre(notasBimestre1[0],notasBimestre1[1],notasBimestre1[2],notasBimestre1[3]);

console.log('1ª nota: ' + bimestre.nota1);
console.log('2ª nota: ' + bimestre.nota2);
console.log('3ª nota: ' + bimestre.nota3);
console.log('4ª nota: ' + bimestre.nota4);

console.log("Somas das notas: " + bimestre.soma());
console.log("Somas das notas: " + bimestre.soma(...notasBimestre1));
