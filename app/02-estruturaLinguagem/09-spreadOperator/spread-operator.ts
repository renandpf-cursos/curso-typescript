function soma(nota1: number, nota2: number, nota3: number, nota4: number): number{
  return nota1 + nota2 + nota3 + nota4;
}

let notasBimestre1 = [7, 10, 9, 8];
let notasBimestre2 = [15, 11, 4, 8];
console.log('Soma das notas: '+ soma(...notasBimestre1));//Ocorre erro na IDE, mas compila e funciona normalmente.


//Não confundir. O exemplo abaixo cria um objeto no estilo json


let notas = {...notasBimestre1 ,...notasBimestre2};
console.log(notas);
console.log('Notas: '+JSON.stringify(notas));//Notas: {"0":15,"1":11,"2":4,"3":8}


let notas2 = {notasBimestre1 ,notasBimestre2};
console.log(notas2);
console.log('Notas: '+JSON.stringify(notas2));//Notas: {"notasBimestre1":[7,10,9,8],"notasBimestre2":[15,11,4,8]}
