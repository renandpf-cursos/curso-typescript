class Pessoa{


  //Deve sempre usar com "..." e deve ser o ultimo parâmetro do método
  public restParameter(idade:number, ...nomes:string[]){
      console.log( "Idade: " + idade );
      console.log( "Nomes: " + nomes );
  }

  public spreadOperators(salario1:number, salario2:number){
    console.log("Salarios: "+(salario1 + salario2));
  }

  public static metodoEstatico():string{
    return "Texto";
  }

}

let p:Pessoa = new Pessoa();
p.restParameter(50,"PrimeiroNome","SegundoNome","TerceiroNome");

let salarios:number[] = [10,15];
//p.spreadOperators(...salarios);//Ocorre erro na IDE, mas compila e funciona normalmente.

console.log(Pessoa.metodoEstatico);
