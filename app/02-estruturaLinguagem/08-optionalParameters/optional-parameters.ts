function add(num1: number, num2: number, num3?: number): number{
  console.log(num3);//Se num3 não for passado, retorna: "num3 is undefined"
  if(isNaN(num3)){
    return num1 + num2;
  }

  return num1 + num2 + num3;
}

console.log(add(1,2,3));//Returna 6
console.log(add(1,2));//Returna 3
//console.log(add(1));////Ocorre erro na IDE, mas o JS é gerado. Se executado retorna "NaN".
