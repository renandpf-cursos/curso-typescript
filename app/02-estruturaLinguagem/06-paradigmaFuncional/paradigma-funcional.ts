/*
*  Como o TS é um "superset" do java script (JS), é permitido utilizar javascript (paradigma funcional) no código TypeScript (TS)
*  É possivel mesclar conceitos do TS (tipagem como "number", por exemplo) com JS (function)
*/
function addNumType(num1: number, num2: number): number{
  return num1 + num2;
}

/*
* "any" aceita qualquer tipo de objeto. Deve ser utilizado com atenção.
*
*/
function addNums(num1: any, num2: number): number{
  if(typeof num1 == 'string'){
    if(isNaN(parseInt(num1,10))){
      return 0;
    }
    num1 = parseInt(num1,10);
  }

  return num1 + num2;
}

console.log(addNums('A',50));//Imprime 0
console.log(addNums('10',50));//Imprime 60
