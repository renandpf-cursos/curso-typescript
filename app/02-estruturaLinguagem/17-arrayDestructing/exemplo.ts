//Array destructing
var x=1,y=2;
[x,y] = [y,x];
console.log(x,y);

var [a,,c] = [1,2,3];//Ignora o "2"
console.log(a,c);

var [d,...resto] = [1,2,3];
console.log(d,resto);//1,[2,3]
