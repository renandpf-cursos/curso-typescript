
//Criando uma variavel no estilo json
var variavelJson = {x: 0, y: 5, width:10, height:15};
//console.log(variavelJson.x);

//Descontruindo: cada elemento dentro das chaves é uma variavel que pode ser utilizadas.
//As variáves deve conter o mesmo nome. A ordem não é obrigatória.
var {x, y, width, height} = variavelJson;
//var {x, y} = variavelJson;//Também funciona


class Pessoa{

  constructor(public nome:string, public telefone:string, public cpf:string){

  }

}

let pessoa:Pessoa = new Pessoa("Fulano","027","088");
let {nome, telefone, cpf} = pessoa;

console.log(nome);
console.log(telefone);
console.log(cpf);
