class Ponto{
  private _x:number = 0;
  private _y:number = 0;

  constructor(p:{x:number,y:number}){
    this._x = p.x;
    this._y = p.y;
  }
}

let pontoJson = {x:10,y:15};
let ponto:Ponto = new Ponto(pontoJson);
console.log(ponto);

//---------------

interface UnidadeMedida{
  distancia:number;
}
class Kilometro implements UnidadeMedida{
  distancia:number;
}

class Javali{
  public mover(distancia:UnidadeMedida):void{
    console.log("Javali andou: "+distancia);
  }
}


let javali:Javali = new Javali();
let distanciaJson = {distancia:15};
javali.mover(distanciaJson);//Passando pametro no formato JSON
