class EstiloOutras{

    //Estilo C#
    private _nome:string;
    private _peso:number;


    set nome(nome:string){
      this.nome = nome;
    }
    get nome():string {
      return this._nome;
    }

    set peso (peso:number){
      this.peso = peso;
    }
    get peso ():number{
      return this._peso;
    }
}

let estiloOutras = new EstiloOutras();
estiloOutras.peso = 10;
estiloOutras.nome = "C#"
