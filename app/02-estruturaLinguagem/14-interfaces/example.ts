interface Animal{
  raca:string;//Pemite atributos
  mover(distancia:number):void;
}



class Minhoca implements Animal{
  raca:string;//Obrigatório existir, assim como os métodos

  public mover(distancia: number){
    console.log("Minhoca rastejou: "+distancia)
  }
}

class Cavalo implements Animal{
  raca:string;

  public mover(distancia: number){
    console.log("Cavalo trotou: "+distancia)
  }
}

class Alien implements Animal {
  raca:string;

  public mover(){
    console.log("Alien voou!");
  }
}


let minhoca:Animal = new Minhoca();
minhoca.mover(15);

let cavalo:Animal = new Cavalo();
cavalo.mover(15);

let alien:Animal = new Alien();
alien.mover(15);


let animal:Animal = new Cavalo();
