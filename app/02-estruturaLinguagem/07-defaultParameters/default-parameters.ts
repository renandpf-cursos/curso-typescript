
//O 3º parametro possui um valor default, no caso da função for chamada sem passar o terceiro parâmetro.
function addNums(num1: number, num2: number, num3: number = 10): number{
  return num1 + num2 + num3;
}


console.log(addNums(1,2,3));//Returna 6
console.log(addNums(1,2));//Returna 13
//console.log(addNums(1));//Ocorre erro na IDE, mas o JS é gerado. Se executado retorna "NaN".
